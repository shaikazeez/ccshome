package com.crestcoresol.iotapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.crestcoresol.iotapp.R;
import com.crestcoresol.iotapp.interfaces.FragmentTransitionListener;
import com.crestcoresol.iotapp.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sysadmin on 13/01/18.
 */

public class CreatePasswordFragment extends BaseFragment{

    @BindView(R.id.button_password_set)
    Button mButtonPasswordSet;

    private FragmentTransitionListener mFragmentTransitionListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_password_fragment,container, AppConstants.FALSE);
        ButterKnife.bind(this,view);
        mFragmentTransitionListener = (FragmentTransitionListener)mBaseActivity;
        return view;
    }

    @OnClick(R.id.button_password_set)
    public void clickEvents(View view){
        //if(view.getId() == R.id.button_set_password){
            mFragmentTransitionListener.fragmentLaunch(AppConstants.THREE);
        //}
    }
}
