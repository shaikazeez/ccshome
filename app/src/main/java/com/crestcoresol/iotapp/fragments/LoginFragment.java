package com.crestcoresol.iotapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.crestcoresol.iotapp.R;
import com.crestcoresol.iotapp.interfaces.FragmentTransitionListener;
import com.crestcoresol.iotapp.model.LoginModel;
import com.crestcoresol.iotapp.network.InternetStatus;
import com.crestcoresol.iotapp.utils.AppConstants;
import com.crestcoresol.iotapp.utils.ErrorUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sysadmin on 13/01/18.
 */

public class LoginFragment extends BaseFragment{

    @BindView(R.id.editText_user_name)
    EditText mEditTextUserName;
    @BindView(R.id.editText_password)
    EditText mEditTextPassword;
    @BindView(R.id.button_signIn)
    Button mButton;

    private Unbinder mUnbinder;
    private FragmentTransitionListener mFragmentTransitionListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment,container, AppConstants.FALSE);
        mUnbinder = ButterKnife.bind(this,view);
        mFragmentTransitionListener = (FragmentTransitionListener)mBaseActivity;
        return view;
    }

    @OnClick({R.id.button_signIn})
    public void clickEvents(View view){
        switch (view.getId()){
            case R.id.button_signIn:
                doLogin();
        }
    }

    private void doLogin() {
        if(InternetStatus.isNetworkAvailable(mBaseActivity)){
            if(!mEditTextUserName.getText().toString().isEmpty() ||
                    !mEditTextPassword.getText().toString().isEmpty()){
                mBaseActivity.showProgressDialog();
                Call<LoginModel> doLogin = mServiceCalls.doLogin(mEditTextUserName.getText().toString(),
                        mEditTextPassword.getText().toString());
                doLogin.enqueue(new Callback<LoginModel>() {
                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        if(response.isSuccessful()){
                            //mBaseActivity.showAlertDialog(response.body().getResponseMessage().toString());
                            mFragmentTransitionListener.fragmentLaunch(AppConstants.THREE);
                            mBaseActivity.closeProgressDialog();
                            mFragmentTransitionListener.fragmentLaunch(AppConstants.THREE);
                        }else {
                            mBaseActivity.showAlertDialog(ErrorUtils.parseError(response).toString());
                            mBaseActivity.closeProgressDialog();
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginModel> call, Throwable t) {
                        mBaseActivity.closeProgressDialog();
                    }
                });

            } else {
                mBaseActivity.showAlertDialog("Username and password should not be empty");
            }
        }else {
            mBaseActivity.showAlertDialog(mBaseActivity.getResources().getString(R.string.no_internet_msg));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }
}
