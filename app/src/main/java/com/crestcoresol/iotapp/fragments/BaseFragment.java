package com.crestcoresol.iotapp.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.crestcoresol.iotapp.activities.BaseActivity;
import com.crestcoresol.iotapp.network.ServiceCalls;
import com.crestcoresol.iotapp.network.ServiceGenerator;

/**
 * Created by sysadmin on 09/01/18.
 */

public class BaseFragment extends Fragment{

    BaseActivity mBaseActivity;
    ServiceCalls mServiceCalls;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseActivity = (BaseActivity)context;
        mServiceCalls = ServiceGenerator.createService(ServiceCalls.class);
    }
}
