package com.crestcoresol.iotapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.crestcoresol.iotapp.R;
import com.crestcoresol.iotapp.interfaces.FragmentTransitionListener;
import com.crestcoresol.iotapp.model.SerialVerificationModel;
import com.crestcoresol.iotapp.utils.AppConstants;
import com.crestcoresol.iotapp.utils.ErrorUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sysadmin on 13/01/18.
 */

public class IPDetailsFragment extends BaseFragment{


    @BindView(R.id.button_set_password)
    Button mButton;
    @BindView(R.id.text_password_cancel)
    TextView mTextViewCancel;
    @BindView(R.id.editText_local_ip)
    EditText mEditTextLocalIP;
    @BindView(R.id.editText_serial_no)
    EditText mEditTextSerialNo;

    private FragmentTransitionListener mFragmentTransitionListener;
    private String mGatewayIP;
    private Bundle mBundle;
    private Unbinder mUnbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ip_details_config_fragment,container, AppConstants.FALSE);
        mUnbinder = ButterKnife.bind(this,view);
        mBundle = getArguments();
        if(null != mBundle){
            mGatewayIP = mBundle.getString(AppConstants.SELECTED_GATEWAY);
        }
        if(null != mGatewayIP){
            mEditTextLocalIP.setText(mGatewayIP);
            mEditTextLocalIP.setEnabled(AppConstants.FALSE);
        }else {
            mEditTextLocalIP.setEnabled(AppConstants.TRUE);
        }
        mFragmentTransitionListener = (FragmentTransitionListener)mBaseActivity;
        return view;
    }

    @OnClick({R.id.text_password_cancel,R.id.button_set_password})
    public void clickEvents(View view){
        switch (view.getId()){
            case R.id.button_set_password:
                verifySerialNo();
                break;
            case R.id.text_password_cancel:

                break;
        }
    }

    private void verifySerialNo(){
        if(mEditTextLocalIP.getText().toString().isEmpty()){
            mEditTextLocalIP.setError("Please enter Local IP");
        }else if(mEditTextSerialNo.getText().toString().isEmpty()){
            mEditTextSerialNo.setError("Please enter Serial Number.");
        }else {
            mBaseActivity.showProgressDialog();
            Call<SerialVerificationModel> call = mServiceCalls.serialNoVerification(mEditTextSerialNo.getText().toString());
            call.enqueue(new Callback<SerialVerificationModel>() {
                @Override
                public void onResponse(Call<SerialVerificationModel> call, Response<SerialVerificationModel> response) {
                    if(response.isSuccessful()){
                        mFragmentTransitionListener.fragmentLaunch(AppConstants.ONE);
                        mBaseActivity.closeProgressDialog();
                    }else {
                        mBaseActivity.showAlertDialog(ErrorUtils.apiErrorResponse(response,mBaseActivity));
                        mBaseActivity.closeProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<SerialVerificationModel> call, Throwable t) {
                    mBaseActivity.closeProgressDialog();
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }
}
