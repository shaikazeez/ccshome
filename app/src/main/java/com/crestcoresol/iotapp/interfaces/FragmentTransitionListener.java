package com.crestcoresol.iotapp.interfaces;

/**
 * Created by sysadmin on 13/01/18.
 */

public interface FragmentTransitionListener {
    void fragmentLaunch(int index);
}
