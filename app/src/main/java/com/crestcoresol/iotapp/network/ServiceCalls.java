package com.crestcoresol.iotapp.network;

import com.crestcoresol.iotapp.model.LoginModel;
import com.crestcoresol.iotapp.model.SerialVerificationModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceCalls {

    /*@Headers("Content-Type: text/plain")
    @GET("Login")
    Call<LoginResponse> doLogin(@QueryMap Map<String, String> loginParams);


    @Headers("Content-Type: text/plain")
    @POST("WhiteLabelContactUs")
    Call<ContactUsModel> whiteLabelContactUs(@Body Map<String, String> UpdateWhiteLabelContactUs);*/

    @POST("mlogin")
    @FormUrlEncoded
    Call<LoginModel> doLogin(@Field("username") String username,
                             @Field("password") String password);

    @FormUrlEncoded
    @POST("verify")
    Call<SerialVerificationModel> serialNoVerification(@Field("serial_no") String serialNo);
}
