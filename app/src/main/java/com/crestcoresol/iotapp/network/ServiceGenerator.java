package com.crestcoresol.iotapp.network;


import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    //Base URL
    public static final String API_BASE_URL = "http://ccsauto.dyndns-web.com:8080/AutomationRest/";

    private static int cacheSize = 10 * 1024 * 1024; // 10 MiB
    private static File cacheDirectory = Environment.getExternalStorageDirectory();
    private static Cache cache = new Cache(cacheDirectory, cacheSize);

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .readTimeout(60, TimeUnit.MINUTES)
            .cache(cache);

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static Retrofit mRetrofit() {
        return builder.client(httpClient.build()).build();
    }

    public static <S> S createService(Class<S> serviceClass) {
        return mRetrofit().create(serviceClass);
    }
}
