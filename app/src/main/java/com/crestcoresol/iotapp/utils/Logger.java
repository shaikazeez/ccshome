package com.crestcoresol.iotapp.utils;

import android.util.Log;

import com.crestcoresol.iotapp.Application;
import com.crestcoresol.iotapp.BuildConfig;


public class Logger {
    public static void e(String msg) {
        if (BuildConfig.IsDebug) {
            Log.e(Application.TAG, msg);
        }
    }

    public static void d(String msg) {
        if (BuildConfig.IsDebug) {
            Log.e(Application.TAG, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (BuildConfig.IsDebug) {
            Log.e(tag, msg);
        }
    }

    public static void w(String msg) {
        if (BuildConfig.IsDebug) {
            Log.w(Application.TAG, msg);
        }
    }

    public static void v(String msg) {
        if (BuildConfig.IsDebug) {
            Log.v(Application.TAG, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.IsDebug) {
            Log.e(tag, msg);
        }
    }

    public static void e(String msg, Throwable t) {
        if (BuildConfig.IsDebug) {
            Log.e(Application.TAG, msg, t);
        }
    }

    public static void e(String tag, String msg, Throwable t) {
        if (BuildConfig.IsDebug) {
            Log.e(tag, msg, t);
        }
    }

    public static void d(String msg, Throwable t) {
        if (BuildConfig.IsDebug) {
            Log.d(Application.TAG, msg, t);
        }
    }

    public static void i(String msg) {
        if (BuildConfig.IsDebug) {
            Log.i(Application.TAG, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (BuildConfig.IsDebug) {
            Log.i(tag, msg);
        }
    }

    public static void i(String tag, String msg, Throwable t) {
        if (BuildConfig.IsDebug) {
            Log.i(tag, msg, t);
        }
    }

    public static void w(String msg, Throwable t) {
        if (BuildConfig.IsDebug) {
            Log.w(Application.TAG, msg, t);
        }
    }

    public static void v(String msg, Throwable t) {
        if (BuildConfig.IsDebug) {
            Log.v(Application.TAG, msg, t);
        }
    }
}
