package com.crestcoresol.iotapp.utils;

/**
 * Created by sysadmin on 09/01/18.
 */

public class AppConstants {

    public static final boolean TRUE = true;
    public static final boolean FALSE = false;
    public static final long SPLASH_TIME_OUT = 000;
    public static final String PREF_NAME = "Css_PreferenceFile";
    public static final String SELECTED_GATEWAY = "SelectedGateway";

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final String RESPONSE_MESSAGE = "responseMessage";
    public static final String FAULT_ERROR = "FaultResponse";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
}
