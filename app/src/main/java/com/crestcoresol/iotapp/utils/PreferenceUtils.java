package com.crestcoresol.iotapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sysadmin on 12/01/18.
 */

public class PreferenceUtils {

    private static Context mContext;

        public PreferenceUtils(Context context) {
            this.mContext = context;
        }

        public static SharedPreferences getPreferences(Context context) {
            return context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE);
        }

        public static SharedPreferences.Editor getEditor(Context context) {
            return getPreferences(context).edit();
        }

        public static void writeBoolean(String key, boolean value) {
            getEditor(mContext).putBoolean(key, value).commit();
        }

        public static boolean readBoolean(String key, boolean defValue) {
            return getPreferences(mContext).getBoolean(key, defValue);
        }

        public static String writeInteger(String key, int value) {
            getEditor(mContext).putInt(key, value).commit();
            return key;
        }

        public static int readInteger(String key, int defValue) {
            return getPreferences(mContext).getInt(key, defValue);
        }

        public static void writeString(String key, String value) {
            getEditor(mContext).putString(key, value).commit();
        }

        public static String readString(String key, String defValue) {
            return getPreferences(mContext).getString(key, defValue);
        }

        public static void writeFloat(String key, float value) {
            getEditor(mContext).putFloat(key, value).commit();
        }

        public static float readFloat(String key, float defValue) {
            return getPreferences(mContext).getFloat(key, defValue);
        }


        public static void writeLong(String key, long value) {
            getEditor(mContext).putLong(key, value).commit();
        }

        public static long readLong(String key, long defValue) {
            return getPreferences(mContext).getLong(key, defValue);
        }

        public static void clearPreference() {
            getEditor(mContext).clear().commit();
        }

}