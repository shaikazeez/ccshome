package com.crestcoresol.iotapp.utils;

import android.content.Context;

import com.crestcoresol.iotapp.R;
import com.crestcoresol.iotapp.model.APIErrorModel;
import com.crestcoresol.iotapp.network.ServiceGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by sysadmin on 15/01/18.
 */

public class ErrorUtils {

    public static APIErrorModel parseError(Response<?> response) {
        Converter<ResponseBody, APIErrorModel> converter =
                ServiceGenerator.mRetrofit()
                        .responseBodyConverter(APIErrorModel.class, new Annotation[0]);

        APIErrorModel error;

        try {
            error = converter.convert(response.errorBody());
        } catch (Exception e) {
            return new APIErrorModel();
        }

        return error;
    }

    public static String apiErrorResponse(Response<?> response, Context context) {
        if (null != response) {
            try {
                JSONObject jObjError = new JSONObject(response.errorBody().string());
                if (jObjError.has(AppConstants.RESPONSE_MESSAGE)) {
                    return jObjError.getString(AppConstants.RESPONSE_MESSAGE);
                } else {
                    return jObjError.getJSONObject(AppConstants.FAULT_ERROR).getString(AppConstants.RESPONSE_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }
        return (context.getResources().getString(R.string.something_wrong));
    }

}

