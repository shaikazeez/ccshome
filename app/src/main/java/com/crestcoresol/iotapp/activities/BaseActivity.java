package com.crestcoresol.iotapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.crestcoresol.iotapp.R;

/**
 * Created by sysadmin on 09/01/18.
 */

public class BaseActivity extends AppCompatActivity{

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    private ProgressDialog mDialog;

    public void showAlertDialog(String message) {
        builder = new AlertDialog.Builder(BaseActivity.this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.setMessage(message);
        alertDialog.show();
    }

    public final void showProgressDialog() {
        try {
            if (null == mDialog) {
                mDialog = new ProgressDialog(BaseActivity.this);
            }
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mDialog.setMessage(getResources().getString(R.string.loading_text));
            mDialog.setCancelable(false);
            mDialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @FunctionalInterface
    interface StringResource{
        public String getResourceString(int id);
    }

    public StringResource stringResource = (int id)->{
      return getResources().getString(id);
    };

    public final void closeProgressDialog() {
        try {
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void hideKeyboardBasedOnView(Context context) {
        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) context).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
