package com.crestcoresol.iotapp.activities;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crestcoresol.iotapp.R;
import com.crestcoresol.iotapp.fragments.CreatePasswordFragment;
import com.crestcoresol.iotapp.fragments.IPDetailsFragment;
import com.crestcoresol.iotapp.fragments.LoginFragment;
import com.crestcoresol.iotapp.interfaces.FragmentTransitionListener;
import com.crestcoresol.iotapp.utils.AppConstants;

import butterknife.BindView;

public class AuthenticationActivity extends BaseActivity implements FragmentTransitionListener{

    private Bundle mBundle;
    private String mGatewayIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        //Extract bundle data
        mBundle = getIntent().getExtras();
        if(!mBundle.isEmpty()){
            mGatewayIP = mBundle.getString(AppConstants.SELECTED_GATEWAY);
        }
        loadFragment(AppConstants.ZERO);
    }

    /**
     * Load fragment
     * @param position - pass fragment type.
     */
    private void loadFragment(int position) {
        Fragment fragment = null;
        Bundle bundle = null;
        switch (position){
            case AppConstants.ZERO:
                fragment = new IPDetailsFragment();
                bundle = new Bundle();
                bundle.putString(AppConstants.SELECTED_GATEWAY,mGatewayIP);
                break;
            case AppConstants.ONE:
                fragment = new LoginFragment();
                break;
            case AppConstants.THREE:
                fragment = new CreatePasswordFragment();
                break;
            /*case AppConstants.FOUR:
                fragment = new IPDetailsFragment();
                break;*/
            default:
        }

        if(null != fragment){
            getSupportFragmentManager()
                    .beginTransaction().
                    replace(R.id.authentication_fragment_container,fragment)
                    .commit();
        }
    }

    @Override
    public void fragmentLaunch(int index) {
        loadFragment(index);
    }
}
