package com.crestcoresol.iotapp.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crestcoresol.iotapp.R;
import com.crestcoresol.iotapp.network.InternetStatus;
import com.crestcoresol.iotapp.utils.AppConstants;
import com.crestcoresol.iotapp.utils.Logger;
import com.crestcoresol.iotapp.utils.PreferenceUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

public class SplashscreenActivity extends BaseActivity {

    private static final String TAG = "SplashscreenActivity";
    private HashSet<String> mGatewayList;
    private String[] mPermissions = new String[]{Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        PreferenceUtils preferenceUtils = new PreferenceUtils(SplashscreenActivity.this);

        if (ContextCompat.checkSelfPermission(SplashscreenActivity.this,
                Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(SplashscreenActivity.this,
                    new String[]{Manifest.permission.INTERNET}, 1);
        } else {
           // Toast.makeText(SplashscreenActivity.this, "you have network accecc", Toast.LENGTH_SHORT).show();
        }

        for(int index = 0;index<mPermissions.length;index++){
            if (ContextCompat.checkSelfPermission(SplashscreenActivity.this,
                    mPermissions[index]) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(SplashscreenActivity.this,
                        mPermissions, index);
            }
        }


       /* if(ContextCompat.checkSelfPermission(SplashscreenActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(SplashscreenActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }else {
            //Toast.makeText(SplashscreenActivity.this, "you have network accecc", Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 0:
                if (grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(SplashscreenActivity.this, "get acess sucess!", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, "you dei the network ", Toast.LENGTH_SHORT).show();
                }
                break;

            case 1:
                if (grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(SplashscreenActivity.this, "Write", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, "you dei the Write ", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        searchGateway.searchGateway();
    }

    @FunctionalInterface
    interface DisplayErrorDialog {
        void displayErrorDialog(String message);
    }

    @FunctionalInterface
    interface SearchGateway {
        void searchGateway();
    }


    DisplayErrorDialog displayErrorDialog = (String message) -> {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(stringResource.getResourceString(R.string.try_again), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                searchGateway.searchGateway();
            }
        });
        alertDialog.setNegativeButton(stringResource.getResourceString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
        });
        alertDialog.create();
        alertDialog.show();
    };

    SearchGateway searchGateway = () -> {
        if (InternetStatus.isNetworkAvailable(SplashscreenActivity.this)) {
            new GateWayDiscoveryClass().execute("");
        } else {
            displayErrorDialog.displayErrorDialog(stringResource.getResourceString(R.string.no_internet_msg));
        }
    };


    //AsyncTask to search gateways
    class GateWayDiscoveryClass extends AsyncTask<String, Void, HashSet<String>> {

        @Override
        protected HashSet<String> doInBackground(String... strings) {
            DatagramPacket receivePacket = null;
            String receivedGateWays = null;
            // Find the server using UDP broadcast
            try {
                if (null == mGatewayList) {
                    mGatewayList = new HashSet<>();
                }
                //Open a random port to send the package
                DatagramSocket socket = new DatagramSocket();
                socket.setBroadcast(true);

                byte[] sendData = "DISCOVER_CCS_GATEWAY_REQUEST".getBytes();
                //Mac
                //Serial No

                //Try the 255.255.255.255 first
                try {
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("255.255.255.255"), 8888);
                    socket.send(sendPacket);
                    Log.d(TAG, getClass().getName() + ">>> Request packet sent to: 255.255.255.255 (DEFAULT)");
                } catch (Exception e) {
                }

                // Broadcast the message over all the network interfaces
                Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();

                    if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                        continue; // Don't want to broadcast to the loopback interface
                    }

                    for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                        InetAddress broadcast = interfaceAddress.getBroadcast();
                        if (broadcast == null) {
                            continue;
                        }

                        // Send the broadcast package!
                        try {
                            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, broadcast, 8888);
                            socket.send(sendPacket);
                        } catch (Exception e) {
                        }

                        Log.d(TAG, getClass().getName() + ">>> Request packet sent to: " + broadcast.getHostAddress() + "; Interface: " + networkInterface.getDisplayName());
                    }
                }

                Log.d(TAG, getClass().getName() + ">>> Done looping over all network interfaces. Now waiting for a reply!");
                socket.setSoTimeout(10000);
                while (true) {
                    //Wait for a response
                    byte[] recvBuf = new byte[15000];
                    receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
                    socket.receive(receivePacket);

                    //We have a response
                    Log.d(TAG, getClass().getName() + ">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());

                    //Check if the message is correct
                    String message = new String(receivePacket.getData()).trim();
                    if (message.equals("DISCOVER_CCS_GATEWAY_RESPONSE")) {
                        //DO SOMETHING WITH THE SERVER'S IP (for example, store it in your controller)
                        //System.out.println("receivePacket.getAddress().toString() = " + receivePacket.getAddress().toString());
                        String gatewayString = receivePacket.getAddress().toString();
                        if(null != gatewayString || !gatewayString.isEmpty()){
                            mGatewayList.add(gatewayString.replace("/", ""));
                        }else {
                            mGatewayList=null;
                        }
                        // receivedGateWays = receivePacket.getAddress().toString();
                        Log.d(TAG, "receivePacket :" + receivePacket.getAddress().toString());
                    }
                }
                //Close the port!
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return mGatewayList;
        }

        @Override
        protected void onPostExecute(HashSet<String> string) {
            super.onPostExecute(string);
            // showMessage(string);
            // mHashSetGatewayList = string.substring(1,string.length()).split("/");
            if (null != mGatewayList && mGatewayList.size()>AppConstants.ZERO) {
                showGatewayList(mGatewayList);
            } else {
                displayErrorDialog.displayErrorDialog(
                        stringResource.getResourceString(
                                R.string.no_gateway_msg));
            }
        }
    }

    /*private void showMessage(String string) {
        Toast.makeText(SplashscreenActivity.this, "Gateways List :" + string, Toast.LENGTH_LONG).show();
        //mTextView.setText(string);
    }*/

    /**
     * Custom Alert Dailog to display Gateway List
     *
     * @param gatewayList - Pass found gateway list in array format
     */
    private void showGatewayList(final HashSet<String> gatewayList) {
        try{
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.gateway_list_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(AppConstants.FALSE);
            final AlertDialog alertDialog1 = alertDialog.create();


            Button buttonSelect = (Button) dialogView.findViewById(R.id.button_select);
            TextView textViewCancel = (TextView) dialogView.findViewById(R.id.text_cancel);
            RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.gateway_radio_group_id);
            final Object[] list = mGatewayList.toArray();
            for (int index = 0; index < list.length; index++) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setText((String) list[index]);
                radioGroup.addView(radioButton);
            }
            alertDialog1.show();
            radioGroup.setOnCheckedChangeListener((radioGroup1, i) -> {
                try {
                    PreferenceUtils.writeString(AppConstants.SELECTED_GATEWAY, (String) list[i - 1]);
                    Logger.d("setOnCheckedChangeListener", "setOnCheckedChangeListener :" + i);
                    //showMessage((String) list[i - 1]);
                } catch (ArrayIndexOutOfBoundsException exception) {
                    Logger.e(TAG, "ArrayIndexOutOfBoundsException ", exception);
                }
            });

            buttonSelect.setOnClickListener(view -> {
                alertDialog1.dismiss();
                //Start next screen
                startActivity(new Intent(SplashscreenActivity.this, AuthenticationActivity.class).
                        putExtra(AppConstants.SELECTED_GATEWAY,
                                PreferenceUtils.readString(
                                        AppConstants.SELECTED_GATEWAY,
                                        "")));
                finish();
            });

            textViewCancel.setOnClickListener(view -> {
                alertDialog1.dismiss();
                finish();
            });
        }catch (Exception exception){
            Logger.d(TAG,"Caught Exception :"+exception);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mGatewayList) {
            mGatewayList.clear();
        }
    }
}