package com.crestcoresol.iotapp.model;

/**
 * Created by sysadmin on 15/01/18.
 */

public class LoginModel {


    /**
     * responseMessage : Login Successful
     */

    private String responseMessage;

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
