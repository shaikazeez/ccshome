package com.crestcoresol.iotapp.model;

/**
 * Created by sysadmin on 15/01/18.
 */

public class SerialVerificationModel {


    /**
     * responseMessage : Device Serial number Verified
     */

    private String responseMessage;

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
